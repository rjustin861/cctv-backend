const express = require('express');
const body_parser = require('body-parser');
const cors = require('cors');
const eventemitter3 = require('eventemitter3');

const ipcamera = require('./dahua');

require('dotenv').config();

const port = process.env.port || 3000;

//Dahua - Options
const options = {
  host: process.env.DAHUA_HOST,
  port: process.env.DAHUA_PORT,
  user: process.env.DAHUA_USER,
  pass: process.env.DAHUA_PASSWORD,
  log: process.env.LOG_LEVEL,
  intrusion_channel: 1,
  fr_channel: 2,
  vehicle_channel: 5
};

const app = express();
const emitter = new eventemitter3();
const dahua = new ipcamera.dahua(options);

//Middleware
app.use(body_parser.json());
app.use(
  body_parser.urlencoded({
    extended: true
  })
);
app.use(cors());

//Dahua - Monitor Camera Alarms
dahua.on('alarm', function(code, data) {
  if (code === 'CrossLineDetection') {
    console.log('CrossLine Event Detected. Data : ' + JSON.stringify(data));
    emitter.emit('alert', JSON.stringify(data));
  }
});

dahua.on('detectFace', function(data) {
  console.log('detectFace Event Detected : ' + JSON.stringify(data));
  if (data.status !== 'Unable To Detect')
    emitter.emit('face', JSON.stringify(data));
});

dahua.on('detectVehicle', function(data) {
  console.log('detectVehicle Event Detected : ' + JSON.stringify(data));
  if (data.status !== 'Unable To Detect')
    emitter.emit('vehicle', JSON.stringify(data));
});

// Callback on connect
dahua.on('connect', function() {
  console.log('connected to dahua');
});

// Callback on error
dahua.on('error', function(error) {
  console.log('error connecting to dahua', error);
});

dahua.on('end', function() {
  console.log('connection ended');
});

//Routes
app.get('/api/alert', (req, res, next) => {
  res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    Connection: 'keep-alive'
  });

  const heartbeat = setInterval(() => {
    res.write('\n');
  }, 5000);

  const onAlert = data => {
    res.write('retry: 500\n');
    res.write('event: alert\n');
    res.write(`data: ${data}\n\n`);
  };

  emitter.on('alert', onAlert);

  req.on('close', () => {
    clearInterval(heartbeat);
    emitter.removeListener('alert', onAlert);
  });
});

app.get('/api/detect', (req, res, next) => {
  res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    Connection: 'keep-alive'
  });

  const heartbeat = setInterval(() => {
    res.write('\n');
  }, 5000);

  const onDetect = data => {
    res.write('retry: 500\n');
    res.write('event: detectFace\n');
    res.write(`data: ${data}\n\n`);
  };

  emitter.on('face', onDetect);

  req.on('close', () => {
    clearInterval(heartbeat);
    emitter.removeListener('face', onDetect);
  });
});

app.get('/api/snapshot/channel/:id', (req, res, next) => {
  //res.writeHead(200, { 'Content-Type': 'application/json' });

  dahua.getSnapshot(req.params.id);

  const onSnapshot = data => {
    console.log('onSnapshot data :', data);
    if (data.status === 'DONE') res.json({ fileUrl: data.fileUrl });
    else res.status(500).end('Server Error');
  };

  dahua.once('getSnapshot', onSnapshot);

  req.on('close', () => {
    dahua.removeListener('getSnapshot', onSnapshot);
  });
});

app.get('/api/image/channel/:id', (req, res, next) => {
  res.writeHead(200, ('Content-Type', 'image/jpg'));

  dahua.getSnapshot(req.params.id);

  const onSnapshot = data => {
    console.log('onSnapshot data :', data);
    if (data.status === 'DONE') res.end(data.responseBody);
    else res.status(500).end('Server Error');
  };

  dahua.once('getSnapshot', onSnapshot);

  req.on('close', () => {
    dahua.removeListener('getSnapshot', onSnapshot);
  });
});

app.get('/api/vehicle', (req, res, next) => {
  res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    Connection: 'keep-alive'
  });

  const heartbeat = setInterval(() => {
    res.write('\n');
  }, 5000);

  const onDetect = data => {
    res.write('retry: 500\n');
    res.write('event: detectVehicle\n');
    res.write(`data: ${data}\n\n`);
  };

  emitter.on('vehicle', onDetect);

  req.on('close', () => {
    clearInterval(heartbeat);
    emitter.removeListener('vehicle', onDetect);
  });
});

app.get('/api/system/info', (req, res, next) => {
  //res.writeHead(200, { 'Content-Type': 'application/json' });
  dahua.getSystemInfo();

  const onSysInfo = data => {
    console.log('System Info :', data);
    res.json(data);
    return;
  };

  dahua.once('getSystemInfo', onSysInfo);

  req.on('close', () => {
    dahua.removeListener('getSystemInfo', onSysInfo);
  });
});

//Errors
app.use(function(error, req, res, next) {
  console.log('error', error);
  res.status(400).json({
    message: error.message
  });
});

app.get('/api/switch/:id', (req, res, next) => {
  //res.writeHead(200, { 'Content-Type': 'application/json' });
  let message = '';

  if (req.params.id === '1') {
    dahua.startFaceDetection();
    message = 'Face Detection Started';
  } else if (req.params.id === '2') {
    dahua.startVehicleDetection();
    message = 'Vehicle Detection Started';
  } else if (req.params.id === '3') {
    dahua.stopFaceAndVehicle();
    message = 'Face and Vehicle Detection Stopped';
  }

  res.status(200).json({ status: 'OK', message });
});

app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});
