# Installation

1. Open a terminal, cd to the folder:

```
cd cctv-backend
```

2. Install all the dependencies:

```
npm install
```

3. Open file .env-sample and fill up the values of dahua camera IP address, port, username, and password
4. Rename .env-sample to .env

```
mv .env-sample .env
```

5. Run index.js from terminal:

```
node index.js
```

# List of APIs

1. GET /api/alert  
   Usage: This API is to listen for any line intrusion event. It must be subscribed by frontend (event name is: alert) to detect intrusion  
   Response: It will return intrusion object info as event-stream

2. GET /api/detect  
   Usage: This API is to listen for facial recognition event. It must be subscribed by frontend (event name is: detectFace) to detect face  
   Response: It will return person info as event-stream

3. GET /api/vehicle  
   Usage: This API is to listen for vehicle detection. It must be subscribed by frontend (event name is: detectVehicle) to detect vehicle  
   Response: It will return vehicle info as event-stream

4. GET /api/switch/:id  
   Usage: This API is to switch on the activation of facial recognition or vehicle detection or switching off both face and vehicle  
   id = 1 is for facial recognition  
   id = 2 is for vehicle detection  
   id = 3 is for stopping both face and vehicle  
   Please take note that there is a 10 second delay from switching on/off the activation

5. GET /api/snapshot/channel/:id  
   Usage: This API is to generate a snapshot image on a server.  
   Request: It accepts channelId as parameter. The channelId is in integer, for example: GET /api/snapshot/channel/1  
   Response: It will return a file path of the snapshot image as JSON. The frontend can use the filepath to grab the image

6. GET /api/image/channel/:id  
   Usage: This API is to generate a snapshot image on a server and return the image snapshot.  
   Request: It accepts channelId as parameter. The channelId is in integer, for example: GET /api/image/channel/1  
   Response: It will return a snapshot image as image/jpg content-type.

7. GET /api/system/info  
   Usage: This API is to return a NVR device info. It is a simple API that is used for troubleshooting connection.  
   Response: It will return NVR device info as JSON

# Sample Codes for Front End

```
if (!!window.EventSource) {
  source1 = new EventSource(`${ALERT_SERVER}/api/alert`);
  source2 = new EventSource(`${ALERT_SERVER}/api/detect`);
  source3 = new EventSource(`${ALERT_SERVER}/api/vehicle`);
} else {
  throw new Error('No EventSource API support');
}

source1.addEventListener(
  'alert',
  function(e) {
    const data = e.data;
    console.log(data);
    content1.innerHTML = data;
  },
  false
);

source2.addEventListener(
  'detectFace',
  function(e) {
    const data = e.data;
    console.log(data);
    content2.innerHTML = data;
  },
  false
);

source3.addEventListener(
  'detectVehicle',
  function(e) {
    const data = e.data;
    console.log(data);
    content3.innerHTML = data;
  },
  false
);
```
