var events = require('events');
var util = require('util');
var request = require('request');
var setKeypath = require('keypather/set');
var fs = require('fs');
var path = require('path');
var moment = require('moment');

// var magic = {
//   jpg: 'ffd8ffe0',
//   png: '89504e47',
//   gif: '47494638'
// };
// var items2 = {};
// var responseBody2 = [];

var log_file = fs.createWriteStream(__dirname + '/debug.log', { flags: 'w' });
var log_stdout = process.stdout;

console.log = function(d) {
  //
  log_file.write(util.format(d) + '\n');
  log_stdout.write(util.format(d) + '\n');
};

var TRACE = true;
var BASEURI = false;
let theRequest = '';
let isCalled = false;

var dahua = function(options) {
  events.EventEmitter.call(this);

  TRACE = options.log;
  BASEURI = 'http://' + options.host + ':' + options.port;
  USER = options.user;
  PASS = options.pass;
  HOST = options.host;

  INTRUSION_CHANNEL = options.intrusion_channel;
  FR_CHANNEL = options.fr_channel;
  VEHICLE_CHANNEL = options.vehicle_channel;

  if (options.cameraAlarms === undefined) {
    options.cameraAlarms = true;
  }

  if (options.cameraAlarms) {
    this.client = this.connect(options);
  }

  this.on('error', function(err) {
    console.log('Error: ' + err);
  });
};

util.inherits(dahua, events.EventEmitter);

dahua.prototype.connect = function() {
  var self = this;

  startLineDetection(self);
  this.startFaceDetection(self);
  this.startVehicleDetection(self);
};

function startLineDetection(self) {
  var opts = {
    url:
      BASEURI +
      `/cgi-bin/eventManager.cgi?action=attach&codes=[AlarmLocal%2CVideoMotion%2CVideoLoss%2CVideoBlind%2CCrossLineDetection]&channel=${INTRUSION_CHANNEL}&heartbeat=30`,
    forever: true,
    headers: { Accept: 'multipart/x-mixed-replace' }
  };

  console.log('Line detection:' + opts.url);

  request(opts)
    .auth(USER, PASS, false)
    .on('response', function() {
      self.emit('connect');
    })
    .on('data', function(data) {
      handleDahuaEventData(self, data);
    })
    .on('error', function(err) {
      if (TRACE) console.log('Connection error: ' + err);
      self.emit('error', err);
    })
    .on('close', function() {
      // Try to reconnect after 30s
      setTimeout(function() {
        self.connect();
      }, 30000);

      if (TRACE) console.log('Connection closed!');
      self.emit('end');
    });
}

dahua.prototype.startFaceDetection = function() {
  var self = this;
  var opts2 = {
    url:
      BASEURI +
      `/cgi-bin/snapManager.cgi?action=attachFileProc&Flags[0]=Event&Events=[FaceRecognition]&channel=${FR_CHANNEL}&heartbeat=10`,
    forever: true,
    headers: { Accept: 'multipart/x-mixed-replace' }
  };

  console.log('Initiate detectFace API. URL:' + opts2.url);

  if (theRequest) {
    console.log('Termintaing request');
    theRequest.abort();
  }

  setTimeout(function() {
    theRequest = request(opts2)
      .auth(USER, PASS, false)
      .on('response', function(response) {
        console.log('Response detecting face : ' + response);
      })
      .on('error', function(err) {
        console.log('Error detecting face : ' + err);
        self.emit('detectFace', { status: 'Unable To Detect' });
      })
      .on('data', function(data) {
        if (TRACE) console.log('Data Snapshot: ' + data.toString());

        // var magigNumberInBody = data.toString('hex', 0, 4);
        // if (
        //   magigNumberInBody == magic.jpg ||
        //   magigNumberInBody == magic.png ||
        //   magigNumberInBody == magic.gif
        // ) {
        //   responseBody2.push(data);
        // } else {
        var items = {};
        var body = data.toString().split('\r\n');
        // parsing items
        body.forEach(function(item) {
          if (
            item.startsWith('Events[0].Face') ||
            item.startsWith('Events[0].Candidates')
          ) {
            item = item.replace(/[[\]]/g, '');
            var propertyAndValue = item.split('=');
            var attribute = propertyAndValue[0].split('.').pop();
            var value = propertyAndValue[1];
            setKeypath(items, attribute, value);
          }
        });
        //}
        if (Object.keys(items).length !== 0) {
          //items2.image = responseBody2;
          console.log('items : ' + items);
          self.emit('detectFace', items);
        } else {
          console.log('No face detected. Only heartbeat');
          self.emit('detectFace', { status: 'Unable To Detect' });
        }
      })
      .on('end', function() {
        console.log('Ending detecting face');
      })
      .on('close', function() {
        console.log('Closing detecting face');
        self.emit('detectFace', { status: 'Unable To Detect' });
      });
  }, 10000);
};

dahua.prototype.startVehicleDetection = function() {
  var self = this;
  var opts3 = {
    url:
      BASEURI +
      `/cgi-bin/snapManager.cgi?action=attachFileProc&Flags[0]=Event&Events=[TrafficJunction]&channel=${VEHICLE_CHANNEL}&heartbeat=10`,
    forever: true,
    headers: { Accept: 'multipart/x-mixed-replace' }
  };

  console.log('Initiate detectVehicle API. URL:' + opts3.url);

  if (theRequest) {
    console.log('Termintaing request');
    theRequest.abort();
  }

  setTimeout(function() {
    theRequest = request(opts3)
      .auth(USER, PASS, false)
      .on('response', function(response) {
        console.log('Response detectVehicle : ' + response);
      })
      .on('error', function(err) {
        console.log('Error detectVehicle : ' + err);
        self.emit('detectVehicle', { status: 'Unable To Detect' });
      })
      .on('data', function(data) {
        if (TRACE) console.log('Data Snapshot: ' + data.toString());

        var items2 = {};
        var body2 = data.toString().split('\r\n');
        // parsing items
        body2.forEach(function(item) {
          if (
            item.startsWith('Events[0].Object') ||
            item.startsWith('Events[0].TrafficCar')
          ) {
            item = item.replace(/[[\]]/g, '');
            var propertyAndValue = item.split('=');
            var attribute = propertyAndValue[0].split('.').pop();
            var value = propertyAndValue[1];
            setKeypath(items2, attribute, value);
          }
        });
        if (Object.keys(items2).length !== 0) {
          console.log('Vehicle detected : ' + items2);
          self.emit('detectVehicle', items2);
        } else {
          console.log('No vehicle detected. Only heartbeat');
          self.emit('detectVehicle', { status: 'Unable To Detect' });
        }
      })
      .on('end', function() {
        console.log('Ending detectVehicle');
      })
      .on('close', function() {
        console.log('Closing detectVehicle');
        self.emit('detectVehicle', { status: 'Unable To Detect' });
      });
  }, 10000);
};

dahua.prototype.stopFaceAndVehicle = function() {
  var self = this;

  if (theRequest) {
    console.log('Termintaing request');
    theRequest.abort();
  }

  setTimeout(function() {
    return;
  }, 10000);
};

function handleDahuaEventData(self, data) {
  if (TRACE) console.log('Data: ' + data.toString());

  data = data.toString().split('\r\n');
  var i = Object.keys(data);
  var imageUrl = '';
  var jsonObj = null;

  i.forEach(function(id) {
    if (data[id].startsWith('Code=CrossLineDetection')) {
      var alarm = data[id].split(';');
      var code = alarm[0].substr(5);
      var action = alarm[1].substr(7);
      if (action == 'Start') jsonObj = JSON.parse(alarm[3].substr(5)).Object;

      //get the image
      if (code === 'CrossLineDetection' && action == 'Start' && !isCalled) {
        self.getSnapshot(INTRUSION_CHANNEL);
      }
    }
  });

  self.once('getSnapshot', data => {
    console.log('onSnapshot data :', data);
    if (data.status === 'DONE') {
      imageUrl = data.fileUrl;
      console.log('imageUrl : ' + imageUrl);

      jsonObj = { ...jsonObj, imageUrl };
      self.emit('alarm', 'CrossLineDetection', jsonObj);
    }
  });
}

function handleFaceData(self, data) {
  //if (TRACE) console.log('Data Snapshot: ' + data.toString());
  var items = {};
  var body = data.toString().split('\r\n');

  // parsing items
  body.forEach(function(item) {
    if (
      item.startsWith('Events[0].Face') ||
      item.startsWith('Events[0].Candidates')
    ) {
      var propertyAndValue = item.split('=');
      var attribute = propertyAndValue[0].split('.').pop();
      var value = propertyAndValue[1];

      setKeypath(items, attribute, value);
    }
  });

  if (Object.keys(items).length !== 0) {
    console.log('items : ' + items);
    self.emit('detectFace', items);
  } else {
    console.log('No face detected. Only heartbeat');
    self.emit('detectFace', { status: 'Unable To Detect' });
  }
}

dahua.prototype.getSnapshot = function(channel) {
  isCalled = true;

  var self = this;

  var filepath = '';
  var filename = this.generateFilename(channel, moment(), '', 'jpg');
  var fileUrl = path.join(filepath, filename);

  var deletefile = false;

  var file = fs.createWriteStream(fileUrl);

  file.on('finish', () => {
    if (deletefile) {
      isCalled = false;
      self.emit('getSnapshot', {
        status: 'FAIL ECONNRESET or 0 byte recieved.'
      });
      console.log(
        moment().format(),
        'FAIL ECONNRESET or 0 byte recieved. Deleting ',
        fileUrl
      );
      fs.unlink(fileUrl, err => {
        if (err) throw err;
      });
    }
  });

  var ropts = {
    uri: BASEURI + '/cgi-bin/snapshot.cgi?channel=' + channel
  };

  console.log('Initiate getsnapshot API. URL: ' + ropts.uri);

  var responseBody = [];
  var responseHeaders = [];

  request(ropts)
    .auth(USER, PASS, false)
    .on('data', chunk => {
      responseBody.push(chunk);
    })
    .on('response', function(response) {
      responseHeaders = response.headers;
    })
    .on('end', function() {
      responseBody = Buffer.concat(responseBody);
      responseBodyLength = Buffer.byteLength(responseBody);

      // check if content-length header matches actual recieved length
      if (responseHeaders['content-length'] != responseBodyLength) {
        isCalled = false;
        self.emit('getSnapshot', 'WARNING content-length missmatch');
      }

      // empty?
      if (responseHeaders['content-length'] == 0) {
        console.log(moment().format(), 'NOT OK content-length 0');
        deletefile = true;
        file.end();
      } else {
        // console.log(moment().format(),'OK content-length',responseBodyLength);
        deletefile = false;
        isCalled = false;
        self.emit('getSnapshot', { status: 'DONE', fileUrl, responseBody });
      }
    })
    .on('error', function(error) {
      self.emit('error', 'ERROR ON SNAPSHOT - ' + error.code);
      deletefile = true;
      isCalled = false;
      file.end();
    })
    .pipe(file);
};

dahua.prototype.getSystemInfo = function() {
  var self = this;

  var ropts = {
    uri: BASEURI + `/cgi-bin/magicBox.cgi?action=getSystemInfo`
  };

  console.log('Initiate getSystemInfo API. URL: ' + ropts.uri);

  request(ropts)
    .auth(USER, PASS, false)
    .on('data', function(data) {
      var items = {};
      var body = data.toString().split('\r\n');

      // parsing items
      body.forEach(function(item) {
        var propertyAndValue = item.split('=');
        var attribute = propertyAndValue[0];
        var value = propertyAndValue[1];

        setKeypath(items, attribute, value);
      });

      self.emit('getSystemInfo', items);
    })
    .on('response', function(res) {
      console.log('Response getSystemInfo :' + res);
    })
    .on('end', function() {
      console.log('Ending getSystemInfo');
    })
    .on('error', function(error) {
      console.log('Error on getSystemInfo emitter:' + error);
    });
};

dahua.prototype.generateFilename = function(channel, start, end, filetype) {
  filename = 'ch' + channel + '_';

  startDate = moment(start, 'YYYYMMDDhhmmss');

  filename += startDate.format('YYYYMMDDhhmmss');
  if (end) {
    endDate = moment(end, 'YYYYMMDDhhmmss');
    filename += '_' + endDate.format('YYYYMMDDhhmmss');
  }
  filename += '.' + filetype;

  return filename;
};

String.prototype.startsWith = function(str) {
  return this.slice(0, str.length) == str;
};

exports.dahua = dahua;
